<?php

use Illuminate\Database\Seeder;

class CostumersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('costumers')->insert(
            [
                [
                    'name' => 'John L',
                    'email' => 'd@d.com',
                    'phone' => '0508877457',
                    'user_id' => '2',
                                      
                ],
                [
                    'name' => 'Cost 2',
                    'email' => 'e@e.com',
                    'phone' => '0508873457',
                    'user_id' => '2',
                                                        
                ],
                [
                    'name' => 'Laravel',
                    'email' => 'f@f.com',
                    'phone' => '0508977457',
                    'user_id' => '3',
                                                      
                ],
            ]);
    }
}
