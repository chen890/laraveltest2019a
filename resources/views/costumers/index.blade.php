@extends('layouts.app')

@section('content')

<h1> Costumers List: </h1>
<a href="{{route('costumers.create')}}"> Create New Costumer</a>
<br>
<ul>
    @foreach($costumers as $costumer)
    <li>
      Name:     {{$costumer->name}} 
      Phone:        {{$costumer->phone}} 
      Email:        {{$costumer->email}}
      <a href="{{route('costumers.edit' , $costumer->id)}}">  Edit    </a>
      @if ($costumer->user_id == 1)
        Creater Name:       'Manager'
        @endif
        @if ($costumer->user_id == 2)
        Creater Name:       'Mike'
        @endif
        @if ($costumer->user_id == 3)
        Creater Name:       'Micheal'
      @endif
      @if ($costumer->status == 0)      
            @can('manager')
            <a href="{{route('statusUpdate', $costumer->id)}}"> Deal Closed </a>
            @endcan
            @else
            <span class="badge badge-success">  Deal Closed: {{$costumer->name}} </span>
        @endif


   
  @can('manager')
            <a href="{{route('delete', $costumer->id)}}">   Delete </a> 
  @endcan        
            
 


    </li>
    @endforeach
</ul>

 


@endsection
