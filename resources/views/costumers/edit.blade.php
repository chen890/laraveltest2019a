@extends('layouts.app')
@section('content')
<h1>
   Edit Costumer:
</h1>

<form method='post' action="{{action('CostumerController@update', $costumer->id)}}">
    @csrf
    @method('PATCH')  

    <div class="form-group">

        <label for="title"> Costumer to Update: </label>
        <input type="text" class ="form-control" name='name' value= "{{$costumer->name}}">
        <input type="text" class ="form-control" name='phone' value= "{{$costumer->phone}}">
        <input type="text" class ="form-control" name='email' value= "{{$costumer->email}}">
    </div>

<div class="form-group">
    <input type="submit" class ="form-control" name='submit' value="Update">
</div>

</form>
@endsection