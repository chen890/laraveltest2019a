<?php

namespace App\Http\Controllers;
use App\Costumer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CostumerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        if(Gate::allows('manager'))
        {
            $costumer=Costumer::all();
            return view('costumers.index' , ['costumers'=> $costumer]);
        }

        $costumer=User::find($id)->costumers;
        return view('costumers.index' , ['costumers'=> $costumer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id=Auth::id();
        $costumer=User::find($id)->costumers;
        return view('costumers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $costumer=new Costumer();
        $id=Auth::id();
        $costumer->name = $request->name;
        $costumer->phone = $request->phone;
        $costumer->email = $request->email;
        $costumer->user_id=$id;
        $costumer->status=0; 
        $costumer->save();
        return redirect('costumers');

    }
    public function statusUpdate(Request $request, $id)
    {
        if (!Gate::allows('manager'))
        {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $costumer = Costumer::findOrFail($id);            
        $costumer->status = 1; 
        $costumer -> update($request->except(['_token']));
        $costumer->update();
        return redirect('costumers');  
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $costumer = Costumer::find($id);
     return view('costumers.edit' , compact('costumer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $costumer = Costumer::find($id);
        $costumer -> update($request->except(['_token']));
        $costumer->update();
        return redirect('costumers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('manager')) 
        {
        $costumer = Costumer::findOrFail($id);
        $costumer->delete();
        return redirect('costumers');
        }
        else
        {
            abort(403, "No Admin- Get Out!");
        }

    }
}
