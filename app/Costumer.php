<?php

namespace App;
use App\Task;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
    
    protected $fillable = [
        'name','email','phone'
    ];
    public function users()
    {
        return $this->belongsTo('App\User');
    }




}
